/*
 * File:   GLCD_ST7920.c
 * Author: Talla
 * https://www.youtube.com/watch?v=oX71D4KO_pI
 * Created on 23. Dezember 2016, 16:34
 */


#include <xc.h>
#include "Alteri.h"
#include<stdlib.h>
//#include<htc.h>
#include<string.h>

/*
 * 1    VSS     GND
 * 2    VDD     VCC
 * 3    V0      POT
 * 4    RS      VCC
 * 5    RW      LATB0
 * 6    E       LATB3
 * 7~14 D0~D7   NC
 * 15   PSB     GND
 * 16   NC      NC
 * 17   RST     NC
 * 18   VOUT    NC
 * 19   BLA     VCC(330R)
 * 20   BLK     GND
 *  */

void LCD_AUSGABE( unsigned char RS , unsigned char DATEN );
void LCD_SENDEN(unsigned char DATEN);
void LCD_TEXT(unsigned char x,unsigned char y,const char *DATEN);

void main(void) {
    ADCON0=0x00; //all digital
    ADCON1=0x00; //all digital
    ADCON1bits.PCFG=0x0F;
    TRISB=0x00; //PORTB output
    LCD_AUSGABE(0,0x30);    
    /*Function Set
     * Bit4 = 1: 8-bit interface
     * Bit3 = X
     * Bit2 = 0: basic instruction
     * Bit1 = X
     * Bit0 = X
     */
    LCD_AUSGABE(0,0x01);
    /*Display Clear */
    LCD_AUSGABE(0,0x06);
    /*Entry Mode Set
     * Set cursor position and display shift when doing write or read operation
     */
    LCD_AUSGABE(0,0x0C);
    /*Display Control
     * Bit2 = 1: Display ON
     * Bit1 = 0: Cursor OFF
     * Bit0 = 0: Character Blink OFF
     */
    while(1){  

    LCD_TEXT(0,0,"TEST");  // LCD_TEXT( X Position , Y Position , Text )
    LCD_TEXT(2,1,"Hello");  // LCD_TEXT( X Position , Y Position , Text )
    LCD_TEXT(4,2,"Position");  // LCD_TEXT( X Position , Y Position , Text )  
    LCD_TEXT(6,3,"Movement");  // LCD_TEXT( X Position , Y Position , Text )   
    }
}
//11111010
void LCD_SENDEN(unsigned char DATEN){
    unsigned char k;
    for (k = 8;k>0;k-- ){         
            LATB3 = 1; //Takt an
            LATB0 = (DATEN & (1 << k-1)) >> k-1; // Daten einzeln rausschaufeln 
            //__delay_ms(1); //Wenn n�tig muss eine Vez�gerung rein, damit sich niemand verschluckt.
            LATB3 = 0; //Takt aus      
    }
}

void LCD_AUSGABE( unsigned char RS , unsigned char DATEN ){    
    unsigned char temp;   
    if(RS){   
        temp = 0b11111010;  //0xFA;    //RS = 1
    }   
    else{   
        temp = 0B11111000; //0xF8;    //RS = 0
    }     
    LCD_SENDEN(temp);   
    temp = DATEN & 0b11110000;        //0xF0;   
    LCD_SENDEN(temp);   
    temp = (DATEN<<4) & 0b11110000;   //0xF0;   
    LCD_SENDEN(temp);     
}

void LCD_TEXT(unsigned char x,unsigned char y,const char *DATEN){   
    unsigned char LAENGE, ADRESSE, Z1; 
    y &= 0b00000011; //0x03;  //y < 4   
    x &= 0b00001111; //0x0F;  //x < 16   
    switch( y )   
    {   
        case 0:   
            ADRESSE = 0b10000000;  //0x80  
            break;   
        case 1:   
            ADRESSE = 0b10010000;  //0x90
            break;   
        case 2:   
            ADRESSE = 0b10001000;  //0x88
            break;   
        case 3:   
            ADRESSE = 0b10011000;  //0x98 
            break;   
    }   
    ADRESSE += x/2;   
    LAENGE = strlen(DATEN);
    if(LAENGE > 16){            //Textl�nge auf 16 Zeichen begrenzen!
        LAENGE = 16;    
    }
    LCD_AUSGABE(0,0x30);                 
    LCD_AUSGABE(0,ADRESSE); 
    
    for(Z1=0;Z1<LAENGE;LAENGE -- )   
        LCD_AUSGABE(1,*(DATEN++));  
        LCD_AUSGABE(0,0x0C);
}
